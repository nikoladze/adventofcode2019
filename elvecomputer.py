import numpy as np
from enum import Enum


class ParType(Enum):
    ADDR = 1
    VAL = 2


class Computer:
    def __init__(self, prog):
        self.prog = np.concatenate([prog, np.zeros(1000000, dtype=np.int64)])
        self.pos = 0
        self.relative_base = 0
        self.inputs = []
        self.outputs = []

    def get_parameters(self, pos, npar, partypes=ParType.VAL):
        if not isinstance(partypes, list):
            partypes = [partypes for i in range(npar)]
        pars = []
        op = self.prog[pos]
        op //= 100
        for i in range(npar):
            partype = partypes[i]
            mode = op % 10
            if mode == 0:
                addr = self.prog[pos + i + 1]
            elif mode == 1:
                assert partype == ParType.VAL
                addr = pos + i + 1
            elif mode == 2:
                addr = self.relative_base + self.prog[pos + i + 1]
            else:
                raise Exception(f"Unknown mode {mode}")
            if partype == ParType.VAL:
                pars.append(self.prog[addr])
            else:
                pars.append(addr)
            op //= 10
        return pars

    def add_input(self, input):
        self.inputs.append(input)

    def run(self, input=None, debug=False):
        if debug:
            import pdb

            pdb.set_trace()
        if input is not None:
            self.add_input(input)
        while True:
            op = self.prog[self.pos] % 10
            if op == 9 and (self.prog[self.pos] // 10 % 10) == 9:
                # 99: halt
                break
            elif op == 3:
                # input
                npars = 1
                if len(self.inputs) > 0:
                    self.prog[
                        self.get_parameters(self.pos, npars, partypes=ParType.ADDR)[0]
                    ] = self.inputs.pop(0)
                else:
                    # nedd to provide input and continue running afterwards
                    return self.outputs
            elif op == 4:
                # output
                self.outputs.append(self.get_parameters(self.pos, 1)[0])
                npars = 1
            elif op == 5:
                # jump-if-true
                pars = self.get_parameters(self.pos, 2)
                if pars[0] != 0:
                    self.pos = pars[1]
                    continue
                npars = 2
            elif op == 6:
                # jump-if-false
                pars = self.get_parameters(self.pos, 2)
                if pars[0] == 0:
                    self.pos = pars[1]
                    continue
                npars = 2
            elif op == 7:
                # less-than
                npars = 3
                pars = self.get_parameters(
                    self.pos, npars, partypes=[ParType.VAL, ParType.VAL, ParType.ADDR]
                )
                if pars[0] < pars[1]:
                    self.prog[pars[2]] = 1
                else:
                    self.prog[pars[2]] = 0
                npars = 3
            elif op == 8:
                # equals
                npars = 3
                pars = self.get_parameters(
                    self.pos, npars, partypes=[ParType.VAL, ParType.VAL, ParType.ADDR]
                )
                if pars[0] == pars[1]:
                    self.prog[pars[2]] = 1
                else:
                    self.prog[pars[2]] = 0
            elif op == 1:
                # addition
                npars = 3
                pars = self.get_parameters(
                    self.pos, npars, partypes=[ParType.VAL, ParType.VAL, ParType.ADDR]
                )
                self.prog[pars[2]] = pars[0] + pars[1]
            elif op == 2:
                # multiplication
                npars = 3
                pars = self.get_parameters(
                    self.pos, npars, partypes=[ParType.VAL, ParType.VAL, ParType.ADDR]
                )
                self.prog[pars[2]] = pars[0] * pars[1]
            elif op == 9:
                # adjust relative base
                npars = 1
                self.relative_base += self.get_parameters(self.pos, npars)[0]
            self.pos += npars + 1
        return self.outputs


def test_pos_mode():
    prog = np.array("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9".split(","), dtype=np.int)
    assert Computer(prog).run(0) == [0]
    assert Computer(prog).run(1) == [1]


def test_immediate_mode():
    prog = np.array("3,3,1105,-1,9,1101,0,0,12,4,12,99,1".split(","), dtype=np.int)
    assert Computer(prog).run(0) == [0]
    assert Computer(prog).run(1) == [1]


def test_larger():
    prog = np.array(
        "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,"
        "1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,"
        "999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".split(","),
        dtype=np.int,
    )
    assert Computer(prog).run(6) == [999]
    assert Computer(prog).run(0) == [999]
    assert Computer(prog).run(7) == [999]
    assert Computer(prog).run(8) == [1000]
    assert Computer(prog).run(9) == [1001]
    assert Computer(prog).run(10) == [1001]


def test_quine():
    prog = np.array(
        "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99".split(","),
        dtype=np.int64,
    )
    assert list(Computer(prog).run()) == list(prog)


def test_16digit():
    prog = np.array("1102,34915192,34915192,7,4,7,99,0".split(","), dtype=np.int64)
    assert len(str(Computer(prog).run()[0])) == 16


def test_large_middle():
    prog = np.array("104,1125899906842624,99".split(","), dtype=np.int64)
    assert Computer(prog).run()[0] == 1125899906842624


def test_day9_1():
    prog = np.loadtxt("9/input.txt", delimiter=",", dtype=np.int64)
    assert Computer(prog).run(1) == [3638931938]
