#!/usr/bin/env python
import time
from elvecomputer import Computer
import numpy as np
import sys
import tty
import termios
import os


class Arcade:

    id_map = {
        0: ".",  # empty
        1: "X",  # wall
        2: "#",  # brick
        3: "_",  # paddle
        4: "o",  # ball
    }

    keymap = {
        # neo-layout asd ;)
        "u": -1,
        "i": 0,
        "a": 1,
        "": 0,
    }

    def __init__(self):
        self.computer = Computer(np.loadtxt("input.txt", delimiter=",", dtype=np.int64))
        self.buffer = np.full((25, 41), "$")
        self.score = 0
        self.ball_pos = 0
        self.pad_pos = 0

    def step(self, key):
        self.screen = np.array(self.computer.run(key)).reshape(-1, 3)
        self.draw()
        print(self.score)

    def run(self, autoplay=False):
        # insert coin
        self.computer.prog[0] = 2
        try:
            fd = sys.stdin.fileno()
            oldSettings = termios.tcgetattr(fd)
            scores = []
            inputs = []
            while True:
                if autoplay:
                    diff = self.ball_pos - self.pad_pos
                    if diff < 0:
                        key = -1
                    elif diff > 0:
                        key = 1
                    else:
                        key = 0
                else:
                    # don't wait for \n
                    tty.setcbreak(fd)
                    key = sys.stdin.read(1)
                    key = self.keymap[key]
                termios.tcsetattr(fd, termios.TCSADRAIN, oldSettings)
                self.step(key)
        except (KeyboardInterrupt, KeyError) as e:
            return

    def draw(self):
        for x, y, i in self.screen:
            if (x, y) == (-1, 0):
                self.score = i
                continue
            if self.id_map[i] == "o":
                self.ball_pos = x
            if self.id_map[i] == "_":
                self.pad_pos = x
            self.buffer[y][x] = self.id_map[i]
        self.draw_buffer()

    def draw_buffer(self):
        # clear screen
        print("\033c")
        for line in self.buffer:
            for c in line:
                print(c, end="")
            print("")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Play the arcade game from adventofcode 2019 day 13"
    )
    parser.add_argument("--autoplay", action="store_true")
    args = parser.parse_args()

    arcade = Arcade()
    arcade.run(autoplay=args.autoplay)
