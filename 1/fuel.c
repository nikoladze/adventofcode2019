#include <stdio.h>
#include <sys/time.h>

int get_fuel(int input_mass) {
  int fuel = input_mass / 3 - 2;
  if (fuel <= 0) {
    return 0;
  } else {
    return fuel + get_fuel(fuel);
  }
}

long getMicrotime() {
  struct timeval currentTime;
  gettimeofday(&currentTime, NULL);
  return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

int main() {
  /* hard coded, no checks ;) */
  int i = 0;
  int nentries = 100;
  int masses[nentries];
  int total_fuel = 0;

  /* read in values */
  FILE *fp = fopen("input.txt", "r");
  while (fscanf(fp, "%d", &masses[i++]) != EOF);

  /* run benchmark */
  int ntrials = 100000;
  long t0 = getMicrotime();
  for (int itrial = 0; itrial < ntrials; itrial++) {
    total_fuel = 0;
    for (int i = 0; i < nentries; i++) {
      total_fuel += get_fuel(masses[i]);
    }
  }
  long t1 = getMicrotime();
  float average = (float)(t1 - t0) / (float)ntrials;

  printf("%f microseconds on average\n", average);
  printf("%d\n", total_fuel);
}
